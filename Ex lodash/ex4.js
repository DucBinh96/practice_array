const _ = require('../lib/lodash');
const { arrFamily } = require('../data');

const TYPE_NUMBER = 'number';
const TYPE_STRING = 'string';
const TYPE_BOOLEAN = 'boolean';

const arrNumber = _.filter(arrFamily, ele => typeof ele === TYPE_NUMBER);
const arrString = _.filter(arrFamily, ele => typeof ele === TYPE_STRING);
const arrBoolean = _.filter(arrFamily, ele => typeof ele === TYPE_BOOLEAN);

console.log(`Array number: ${arrNumber}`);
console.log(`Array string: ${arrString}`);
console.log(`Array boolean: ${arrBoolean}`);