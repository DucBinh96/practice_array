const _ = require('../lib/lodash');
const {
  arrStudent,
  arrProvince
} = require('../data');

// {
//   name: 'Van',
//   address: 'HungYen',
//   age: 24,
//   country: 'VN',
//   income: 120,
//   married: false,
//   gender: 'female'
// },

// {
//   regions: 'North',
//   province: [
//     'HaNoi',
//     'BacNinh',
//     'HungYen'
//   ]
// },

// 1
const ID_VIETNAM = 'VN';
const [arrStudentInVN, arrStudentNotInVN] =  _.partition(arrStudent, student => student.country === ID_VIETNAM);

console.log(`Student in VN: `);
console.log(arrStudentInVN)
console.log(`Student not in VN: `);
console.log(arrStudentNotInVN)

// 2
const [arrStudentMarried, arrStudentNotMarried] = _.partition(arrStudent, student => student.married); 

console.log(`Student married:`);
console.log(arrStudentMarried)
console.log(`Student not married: `);
console.log(arrStudentNotMarried)

// 3
// const CHARACTER_CHECK_ADDRESS = 'ha'; // include upper and lower case
// const checkAddress = function(address) {
//   return address ? false : _.toLower(address).indexOf(CHARACTER_CHECK_ADDRESS) != -1;
// }
// const arrStudentAddressIncludeCharacter = arrStudent.filter(student => checkAddress(student.address));
// console.log(`Student have address include ha: `);
// console.log(arrStudentAddressIncludeCharacter)

// 4
const MAX_LENGTH_OF_NAME = 5;
const arrStudentFilterByNameLength = _.filter(arrStudent, student => student.name.length <= MAX_LENGTH_OF_NAME);
console.log(`Student have name length max 5: `);
console.log(arrStudentFilterByNameLength)

// 5
const MIN_INCOME = 200;
const arrStudentMinIncome = _.filter(arrStudent, student => student.income >= MIN_INCOME);
console.log(`Student have name min income 200: `);
console.log(arrStudentMinIncome)

// 6
const MIN_AGE = 22;
const MAX_AGE = 26;
const arrStudentFilterByAge = _.filter(arrStudent, student => student.age > MIN_AGE && student.age < MAX_AGE);
console.log(`Student have age >22 and <26: `);
console.log(arrStudentFilterByAge)

// 7
const NORTH = "North"
const north = _.find(arrProvince, province => province.regions === NORTH);
const arrStudentLiveNorth = _.filter(arrStudentInVN, student => north.province.includes(student.address));
console.log(`Student live in the north of VN: `);
console.log(arrStudentLiveNorth)

// 8
const MIN_AGE_FEMALE = 26;
const FEMALE = 'female';
const arrStudentFemale26NotMarried = _.filter(arrStudentNotMarried, student => student.age > MIN_AGE_FEMALE && student.gender === FEMALE);
console.log(`Student is female - 26 - not married: `);
console.log(arrStudentFemale26NotMarried)

// 9
const arrStudentLiveNorthNotMarried = _.filter(arrStudentLiveNorth, student => !student.married);
console.log(`Student live north and not married: `);
console.log(arrStudentFemale26NotMarried)

// 10
const calculateFee = function(student) {
  if (student.income < 200) return student.age * student.income + 1000;
  if (student.income > 250) return student.age * student.income + 1500;
}
const arrStudentCalIncome = _.map(arrStudent, student => ({
  ...student,
  name: student.name.toUpperCase(),
  fee: calculateFee(student)
}))
console.log(`Student after calculate fee: `);
console.log(arrStudentCalIncome)