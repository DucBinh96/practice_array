const _ = require('../lib/lodash');
const { arrFamily } = require('../data');

const TYPE_NUMBER = 'number';
const TYPE_STRING = 'string';
const CHARACTER_JOIN_STRING = ',';

const arrNumber = _.filter(arrFamily, ele => typeof ele === TYPE_NUMBER);
const arrString = _.filter(arrFamily, ele => typeof ele === TYPE_STRING);
const sumOfArrNumber = _.reduce(arrNumber, (sum, currentValue) => sum + currentValue);
const stringJoin = _.join(arrString, CHARACTER_JOIN_STRING);

console.log(`Sum of array number: ${sumOfArrNumber}`);
console.log(`String join of array string: ${stringJoin}`);