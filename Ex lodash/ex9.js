const _ = require('../lib/lodash');
const {
  arrStudent,
  arrProvince
} = require('../data');

const regionChoice = function(student) {
  return _.find(arrProvince, province => province.province.includes(student.address));
}
const arrStudentAddProvince = _.map(arrStudent, student => ({
  ...student,
  region: regionChoice(student)
}))
console.log('Student add region: ');
console.log(arrStudentAddProvince);