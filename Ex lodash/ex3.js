const _ = require('../lib/lodash');
const { arrAge } = require('../data');

const sum = _.reduce(arrAge, (sum, currentEle) => sum + currentEle);
console.log(`Sum: ${sum}`);