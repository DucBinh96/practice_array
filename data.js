const arrAge = [15, 16, 17, 18, 19, 20];

const arrFamily = ['Vân', 24, 20, 'Thịnh', true, false];

const arrStudent = [
  {
    name: 'Van',
    address: 'HungYen',
    age: 24,
    country: 'VN',
    income: 120,
    married: false,
    gender: 'female'
  },
  {
    name: 'Thinh',
    address: 'DaNang',
    age: 20,
    country: 'VN',
    income: 70,
    married: true,
    gender: 'male'
  },
  {
    name: 'HoaPham',
    address: 'HaNoi',
    age: 26,
    country: 'VN',
    income: 150,
    married: true,
    gender: 'male'
  },
  {
    name: 'Mr.John',
    address: 'NewYork',
    age: 25,
    country: 'US',
    income: 300,
    married: true,
    gender: 'male'
  },
  {
    name: 'Marry',
    address: 'Manchester',
    age: 27,
    country: 'UK',
    income: 310,
    married: false,
    gender: 'female'
  }
];

const arrProvince = [
  {
    regions: 'North',
    province: [
      'HaNoi',
      'BacNinh',
      'HungYen'
    ]
  },
  {
    regions: 'Southern',
    province: [
      'Tp.HoChiMinh',
      'VungTau',
      'CaMau'
    ]
  }
];

module.exports = {
  arrAge,
  arrFamily,
  arrStudent,
  arrProvince
}