let {
  arrAge,
} = require('../data');

// For
let resultByFor = [];
for (let i = 0; i < arrAge.length; i++) {
  if (!(arrAge[i]%2)) resultByFor.push(arrAge[i]);
}
console.log(`Result use for: ${resultByFor}`);

// For of
let resultByForOf = [];
for (let ele of arrAge) {
  if (!(ele%2)) resultByForOf.push(ele);
}
console.log(`Result use for of: ${resultByForOf}`);

// For each
let resultByForEach = [];
arrAge.forEach(ele => {
  if (!(ele%2)) resultByForEach.push(ele);
})
console.log(`Result use for each: ${resultByForEach}`);

// Filter
let resultByFilter = arrAge.filter(ele => !(ele%2));
console.log(`Result use filter: ${resultByFilter}`);