let {
  arrFamily
} = require('../data');

const TYPE_NUMBER = 'number';
const TYPE_STRING = 'string';
const TYPE_BOOLEAN = 'boolean';

let arrNumber = arrFamily.filter(ele => typeof ele === TYPE_NUMBER);
let arrString = arrFamily.filter(ele => typeof ele === TYPE_STRING);
let arrBoolean = arrFamily.filter(ele => typeof ele === TYPE_BOOLEAN);

console.log(`Array number: ${arrNumber}`);
console.log(`Array string: ${arrString}`);
console.log(`Array boolean: ${arrBoolean}`);