const {
  arrStudent
} = require('../data');

// 1
const sumAgeStudent = arrStudent.reduce(((sum, currentEle) => sum + currentEle.age), 0);
console.log(`Sum age student: ${sumAgeStudent}`);

// 2
const sumIncomeStudent = arrStudent.reduce(((sum, currentEle) => sum + currentEle.income), 0);
console.log(`Sum income student: ${sumIncomeStudent}`);

// 3
const MALE = 'male';
const calculateFeeAllowance = function(student) {
  return student.gender === MALE ? 0 : student.age * student.income;
}
const sumOfFeeAllowance = arrStudent.reduce(((sum, currentEle) => sum + calculateFeeAllowance(currentEle)), 0);
console.log(`Sum of Fee Allowance: ${sumOfFeeAllowance}`);
