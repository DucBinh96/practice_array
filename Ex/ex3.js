let {
  arrAge,
} = require('../data');

// For
let sumByFor = 0;
for (let i = 0; i < arrAge.length; i++) {
  sumByFor += arrAge[i];
}
console.log(`sum use for: ${sumByFor}`);

// For of
let sumByForOf = 0;
for (let ele of arrAge) {
  sumByForOf += ele;
}
console.log(`sum use for of: ${sumByForOf}`);

// For each
let sumByForEach = 0;
arrAge.forEach(ele => {
  sumByForEach += ele;
})
console.log(`sum use for each: ${sumByForEach}`);

// Filter
let sumByFilter = arrAge.reduce(((sum, currentEle) => sum + currentEle), 0);
console.log(`sum use filter: ${sumByFilter}`);