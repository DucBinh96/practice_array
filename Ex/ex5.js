const {
  arrFamily
} = require('../data');

const TYPE_NUMBER = 'number';
const TYPE_STRING = 'string';
const CHARACTER_JOIN_STRING = ',';

const arrNumber = arrFamily.filter(ele => typeof ele === TYPE_NUMBER);
const arrString = arrFamily.filter(ele => typeof ele === TYPE_STRING);
const sumOfArrNumber = arrNumber.reduce(((sum, currentValue) => sum + currentValue), 0);
const stringJoin = arrString.join(CHARACTER_JOIN_STRING);

console.log(`Sum of array number: ${sumOfArrNumber}`);
console.log(`String join of array string: ${stringJoin}`);