let {
  arrAge,
} = require('../data');

const AGE_MIN = 18;

// For
let resultByFor = [];
for (let i = 0; i < arrAge.length; i++) {
  if (arrAge[i] >= AGE_MIN) resultByFor.push(arrAge[i]);
}
console.log(`Result use for: ${resultByFor}`);

// For of
let resultByForOf = [];
for (let ele of arrAge) {
  if (ele >= AGE_MIN) resultByForOf.push(ele);
}
console.log(`Result use for of: ${resultByForOf}`);

// For each
let resultByForEach = [];
arrAge.forEach(ele => {
  if (ele >= AGE_MIN) resultByForEach.push(ele);
})
console.log(`Result use for each: ${resultByForEach}`);

// Filter
let resultByFilter = arrAge.filter(ele => ele >= AGE_MIN);
console.log(`Result use filter: ${resultByFilter}`);