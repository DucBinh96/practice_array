const {
  arrStudent,
  arrProvince
} = require('../data');

// 1
const SOUTHERN = "Southern";
const ID_VN = 'VN';
const south = arrProvince.find(province => province.regions === SOUTHERN);
const arrStudentLiveNorth = arrStudent.filter(student => student.country === ID_VN && south.province.includes(student.address));
console.log(`First Student in arr live in the south of VN: `);
console.log(arrStudentLiveNorth[0] || 0)

// 2
const MAX_AGE = 25;
const MIN_INCOME = 200;
const arrStudentFilter = arrStudent.filter(student => student.married && student.age <= MAX_AGE && student.income > MIN_INCOME);
console.log(`Student married, age<25, income>200: `);
console.log(arrStudentFilter)