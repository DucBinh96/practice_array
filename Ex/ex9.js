const {
  arrStudent,
  arrProvince
} = require('../data');

const regionChoice = function(student) {
  return arrProvince.find(province => province.province.includes(student.address));
}
const arrStudentAddProvince = arrStudent.map(student => ({
  ...student,
  region: regionChoice(student)
}))
console.log('Student add region: ');
console.log(arrStudentAddProvince);